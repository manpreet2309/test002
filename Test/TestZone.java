import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestZone {
Zone c = new Zone();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
//R1 A on way trip inside zone 1
	@Test
	public void testOneWayTrip() {
		String[] f = new String[]{"leslie"};
		String[] t = new String[]{"Don Mills"};
		
		double charges =c.calculateTotal(f,t);
		assertEquals(2.5,charges,0.0);	 	
	}
	

//R2 A on way trip inside zone 1
	@Test
	public void testOneWayTripZone2() {
		String[] f = new String[]{"Sheppard"};
		String[] t = new String[]{"Finch Station"};
		
		double charges =c.calculateTotal(f,t);
		assertEquals(3.0,charges,0.0);	 	
	}
	
	//R3  trip between zones
		@Test
		public void testBetweenZones() {
			String[] f = new String[]{"Don Mills"};
			String[] t = new String[]{"Finch Station"};
			
			double charges =c.calculateTotal(f,t);
			assertEquals(3.0,charges,0.0);	 	
		}
		//R4 more than 1 Trip
				@Test
				public void testMoreThanOneTrip() {
					String[] f = new String[]{"Finch","lesile"};
					String[] t = new String[]{"Sheppard","Don Mills"};
					
					double charges =c.calculateTotal(f,t); 
					assertEquals(5.50,charges,0.0);	 
					
				}
			
				//R5 Reaching Daily Maximum
				@Test
				public void testReachingDailyMaximum() {
					String[] f = new String[]{"Finch","Sheppard","Finch"};
					String[] t = new String[]{"Sheppard","Finch","Sheppard"};
					
					double charges =c.calculateTotal(f,t); 
					assertEquals(6.00,charges,0.0);	 
					
				}
}	


